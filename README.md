## StarUML unlock full version

Project created by [Arun Kumar](https://gist.github.com/arkn98/5a52eb910f61a71cbf45948893c571e7)

Here's a complete guide (for newbies) (it worked for me on Ubuntu 18.04.1 LTS):

1. Download the latest StarUML .AppImage (file will be named `StarUML-3.0.2-x86_64.AppImage`) from StarUML's website [staruml.io](http://staruml.io)
2. Then make the downloaded. AppImage executable by running `sudo chmod +x StarUML-3.0.2-x86_64.AppImage`
3. Install npm using apt, if it is not installed `sudo apt install npm`
4. Install asar npm package using `sudo npm install -g asar`
5. If you're using npm for the first time, then you can't directly call `asar` from the terminal. You need to update your `$PATH` variable to include the `.npm-global` directory to directly call globally installed npm packages. This can be done by adding `export PATH="/home/$USER/.npm-global/bin:$PATH"` (may differ based on your config; refer to this SO [answer](https://stackoverflow.com/a/15623632/9523462)) at the end of your `~/.bashrc`. Then, logout and login again.
6. Then extract .AppImage file using `./StarUML-3.0.2-x86_64.AppImage --appimage-extract` (replace the correct version; tab completion will help)
7. `cd squashfs-root/resources/`
8. `asar extract app.asar app`
9. `cd app/src/engine`
10. Edit the `license-manager.js` file. Make the following changes:
```javascript
//... existing code
checkLicenseValidity () {
    this.validate().then(() => {
      setStatus(this, true)
    }, () => {
      //setStatus(this,false)  <-- comment this line
      setStatus(this, true) //<-- add this line
      //UnregisteredDialog.showDialog() <-- comment this line
    })
  }
//... rest of the code
```
11. `cd ../../../`
12. Pack the updated .AppImage using `asar pack app app.asar`
13. Remove the previously extracted `app` directory by running `rm -rf app/*` (BE CAREFUL! Make sure you are on the right directory) followed by `rmdir app`
14. `cd ../../`
15. Download the appimagetool from [here](https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage) to your current directory
16. Then make the downloaded. appimagetool executable by running `sudo chmod +x appimagetool-x86_64.AppImage`
17. Run this `./appimagetool-x86_64.AppImage squashfs-root/` to generate the new .AppImage file
18. Run the new .AppImage file by running `./StarUML-x86_64.AppImage`
